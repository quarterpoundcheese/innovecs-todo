# Innovecs Todo Application Front-end

This is a todo application for Innovecs HR team.

### .env
```
DATABASE_FILENAME=
GIN_MODE=
```
Both must be present

### Architecture
The architecture of the application is extremely simple. There are controllers and services. Controllers parse the incoming request and pass the request to the services. This way TDD is much easier because the tests need to be run on the services
<br/>
<br/>
Each Route has its own service and the service is the same name with the controller's function but with ```Service``` suffix added.
### Gorm
The fantastic ORM library for Golang, aims to be developer friendly.
<br/>
<br/>
I used Gorm because I have experience with it and the fact that I can use hooks like ```BeforeSave()``` 
<br/>

### Gin
Gin is a web framework written in Go. Again, I used Gin because I have past experience with it and code snippets from past projects that enables me to work in a comfortable enviroment

### 

### Deployment
There were many problems with the deployment of the both application (backend, and front-end). I was able to dockerize both applications and create a CI/CD pipeline that was tasked with testing, building, and deploying to the server. <br/>
<br/>
However, I was not able to get this pipeline to work concurrently. Whenever I would commit to main on the front-end application and verified that it is working in production, the backend application that was working before would stop working.
<br/><br/>
Whatever application had the latest pipeline run would be the only application that works in production, the other one would just stop working.
<br/><br/>
After hours and hours of trying to fix the issue, I decided to manually pull the repos from gitlab and run the applications in docker
<br/><br/>
Both applications have their own nginx conf, I realized that this was not really necessary as the host server has nginx set up that acts as a reverse proxy to these applications. I implemented this type of architecture as this way it was really easy for me to get the SSL certificates for the two subdomains
<br/><br/>
```Backend https://innovecsapi.gasimzade.az/api/v1```
<br/>
```Frontend https://innovecs.gasimzade.az```

### Scripts
You can build the application as such
<br/>
```docker build -t frontend .```
<br/>
<br/>
And run it as such
<br/>
```docker run -p ${desired_port}:80 -d frontend```