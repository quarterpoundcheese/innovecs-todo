FROM golang:1.17.1-alpine AS builder
RUN apk add build-base
ENV GOOS=linux

WORKDIR /app

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY . ./

RUN go build -o ./app

FROM nginx:1.16.0-alpine
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/conf.d

COPY --from=builder /app/app .
COPY .env .
CMD ./app
