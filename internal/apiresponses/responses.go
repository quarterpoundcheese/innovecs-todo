package apiresponses

const (
	ValidationError      = "Validation Error"
	MalformedInput       = "Malformed Input"
	OperationSuccessFull = "Operation Successful"
	InternalServerError  = "Internal Server Error"
)
