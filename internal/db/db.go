package db

import (
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"innovecs-test/internal/models"
	"os"
)

var Db *gorm.DB

func Init() {
	dbn, err := gorm.Open(sqlite.Open(os.Getenv("DATABASE_FILENAME")), &gorm.Config{})
	if err != nil {
		panic("Failed to connect database")
	}

	Db = dbn

	if err := Db.AutoMigrate(&models.Todo{}); err != nil {
		panic("Failed to auto migrate")
	}
}
