package models

import (
	"gorm.io/gorm"
	"strings"
)

type Todo struct {
	gorm.Model
	TodoTitle string `gorm:"not null" validate:"required,ascii,min=3,max=100" json:"todo_title" binding:"required"`
}

func (t *Todo) BeforeCreate(tx *gorm.DB) (err error) {
	t.TodoTitle = strings.Trim(t.TodoTitle, " ")
	return nil
}

func NewTodo(title string) *Todo {
	return &Todo{
		TodoTitle: title,
	}
}
