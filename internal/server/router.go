package server

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"innovecs-test/internal/controllers/home"
	"innovecs-test/internal/controllers/todo"
	"os"
)

func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, GET, DELETE, PUT, OPTIONS")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}

func NewRouter() *gin.Engine {

	if os.Getenv("GIN_MODE") == "release" {
		gin.SetMode(gin.ReleaseMode)
	}
	router := gin.New()
	// router.Use(gin.Logger())

	router.Use(gin.Recovery())

	router.Use(cors.Default())

	apiV1 := router.Group("/api/v1")

	home.NewController(apiV1)
	todo.NewController(apiV1)

	return router
}
