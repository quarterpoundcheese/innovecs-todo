package server

import (
	"fmt"
	"log"
)

func Start() {
	r := NewRouter()

	err := r.Run(":4000")

	if err != nil {
		log.Fatal("Something went wrong\n", err)
	}

	fmt.Println("Routes initialized")
}