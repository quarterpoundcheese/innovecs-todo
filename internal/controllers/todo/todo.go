package todo

import (
	"github.com/gin-gonic/gin"
	"innovecs-test/internal/models"
	"innovecs-test/internal/apiresponses"
)

func NewController(r *gin.RouterGroup) *gin.RouterGroup {
	group := r.Group("/todo")
	{
		group.POST("/", CreateTodo)
		group.GET("/", GetAllTodos)
	}

	return group
}

func CreateTodo(c *gin.Context) {
	var todo models.Todo
	if err := c.BindJSON(&todo); err != nil {
		c.JSON(400, gin.H{"msg": apiresponses.MalformedInput})
		return
	}

	status, msg, _ := CreateTodoService(&todo)
	c.JSON(status, gin.H{"msg": msg})
}

func GetAllTodos(c *gin.Context) {
	todos := GetAllTodosService()
	c.JSON(200, todos)
}
