package todo

import (
	"innovecs-test/internal/apiresponses"
	"innovecs-test/internal/db"
	"innovecs-test/internal/models"
	"strconv"
	"testing"
)

func TestTodoService(t *testing.T) {
	testInputs := []struct {
		todoName       string
		expectedStatus int
		expectedMsg    string
	}{
		{
			todoName:       "Buy bread",
			expectedStatus: 200,
			expectedMsg:    apiresponses.OperationSuccessFull,
		},
		{
			todoName:       "b",
			expectedStatus: 400,
			expectedMsg:    apiresponses.ValidationError,
		},
		{
			todoName:       "wJGDihraE9ShIfSGRqF4IKFgYJ1ZoG7bk5iWg9vFMGVYJJEKHJUQwzz0Ax5QefBvAchiXJCFp0GfrAnCbaheEPtiestlyYlyWC2vu",
			expectedStatus: 400,
			expectedMsg:    apiresponses.ValidationError,
		},
		{
			todoName:       "⊂(◉‿◉)つ Confused",
			expectedStatus: 400,
			expectedMsg:    apiresponses.ValidationError,
		},
		{
			todoName:       "💁👌🎍😍",
			expectedStatus: 400,
			expectedMsg:    apiresponses.ValidationError,
		},
		{
			todoName:       "Clean the dishes",
			expectedStatus: 200,
			expectedMsg:    apiresponses.OperationSuccessFull,
		},
	}

	db.Init()

	for i, input := range testInputs {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			todo := models.NewTodo(input.todoName)

			status, msg, _ := CreateTodoService(todo)

			if expectedStatus, expectedMsg, actualMsg, actualStatus := input.expectedStatus, input.expectedMsg, msg, status; (expectedMsg != actualMsg) || (expectedStatus != actualStatus) {
				t.Errorf("Expected values do not match actual values: %#v %#v, %#v %#v", expectedStatus, actualStatus, expectedMsg, actualMsg)
				return
			}

			todos := GetAllTodosService()

			for _, fetchedTodo := range todos {
				if fetchedTodo.TodoTitle == todo.TodoTitle {
					t.Logf("Found added todo in database")
					return
				}
			}
		})
	}
}
