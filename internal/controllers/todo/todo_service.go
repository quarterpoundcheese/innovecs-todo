package todo

import (
	"github.com/go-playground/validator/v10"
	"innovecs-test/internal/apiresponses"
	"innovecs-test/internal/db"
	"innovecs-test/internal/models"
)

func CreateTodoService(todo *models.Todo) (int, string, error) {

	// We have single operation thus custom transaction
	// is not needed

	validate := validator.New()

	if err := validate.Struct(todo); err != nil {
		return 400, apiresponses.ValidationError, err
	}

	if res := db.Db.Create(todo); res.Error != nil {
		return 500, apiresponses.InternalServerError, res.Error
	}

	return 200, apiresponses.OperationSuccessFull, nil
}

func GetAllTodosService() []models.Todo {
	var todos []models.Todo
	db.Db.Order("created_at desc").Find(&todos)
	return todos

}
