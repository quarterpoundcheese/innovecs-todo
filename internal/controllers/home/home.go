package home

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func NewController (r *gin.RouterGroup) *gin.RouterGroup {
	group := r.Group("/")
	{
		group.GET("/", index)
	}

	return group
}

func index(c *gin.Context) {
	c.String(http.StatusOK, "This controller seems to be working")
}