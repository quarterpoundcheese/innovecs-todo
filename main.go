package main

import (
	"fmt"
	"github.com/joho/godotenv"
	"innovecs-test/internal/db"
	"innovecs-test/internal/server"
	"log"
)

func main() {
	err := godotenv.Load()
	db.Init()


	if err != nil {
		log.Fatal("Error loading .env file", err)
	}

	fmt.Println("Welcome to backend service for Innovecs Todo App")
	fmt.Println("Read the attached readme for the instructions")

	server.Start()


	fmt.Println("Server started")
}
